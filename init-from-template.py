#!ipython

import json
from pathlib import Path
from typing import List, Optional

import jinja2  # https://zetcode.com/python/jinja/
import typer  # https://typer.tiangolo.com/
from slugify import slugify

app = typer.Typer(help="Initialize a directory from a template")

env = jinja2.Environment(
    keep_trailing_newline=True,
)
env.filters["slugify"] = slugify


@app.command()
def init(
    template_dir: Path,
    template_settings: Optional[List[str]] = typer.Argument(None),
    no_interact: bool = typer.Option(False, "--no-interact", "-n"),
    allow_not_empty_dir: bool = typer.Option(False, "--allow-not-empty-dir"),
) -> int:
    template_json_file = template_dir / "template.json"
    if not template_json_file.is_file():
        typer.echo(f"Json file {template_json_file} not found", err=True)
        return 1
    template_root_dir = template_dir / "_root"
    if not template_root_dir.is_dir():
        typer.echo(f"Root directory {template_root_dir} not found", err=True)
        return 1
    current_content = list(Path().iterdir())
    if not allow_not_empty_dir and (
        len(current_content) > 1
        or (
            len(current_content) == 1
            and not current_content[0].is_dir()
            and not current_content[0].name != ".git"
        )
    ):
        typer.echo(
            f"Current directory is not empty, only .git folder allowed", err=True
        )
        return 1

    template_dict = json.loads(template_json_file.read_text())

    template_settings = template_settings or []
    for keyvalue in template_settings:
        tokens = keyvalue.split("=")
        if len(tokens) < 2:
            typer.echo(f"Invalid key=value {keyvalue}", err=True)
            return 1
        if len(tokens) > 2:
            tokens = [tokens[0], "=".join(tokens[1:])]
        template_dict[tokens[0]] = tokens[1]

    for name, value in template_dict.items():
        tmpl = env.from_string(value)
        value = tmpl.render(**template_dict)
        if not no_interact:
            value = typer.prompt(f"{name}", default=value)
        template_dict[name] = value

    typer.echo(
        f"Instanciating template with values {json.dumps(template_dict, indent=2)}",
        err=True,
    )

    queue = [
        (src, src.relative_to(template_root_dir)) for src in template_root_dir.iterdir()
    ]
    while len(queue) > 0:
        current, dst = queue[0]
        typer.echo(f"Instanciating {current} => {dst}")
        queue.pop(0)
        if current.is_file():
            tmpl = env.from_string(current.read_text())
            with open(dst, "w", newline="\n") as dst_io:
                dst_io.write(tmpl.render(**template_dict))
        else:
            dst.mkdir(exist_ok=True, parents=True)
            queue.extend(
                [
                    (src, dst / env.from_string(src.name).render(**template_dict))
                    for src in current.iterdir()
                ]
            )

    return 0


if __name__ == "__main__":
    exit(app())
